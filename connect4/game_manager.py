
import time

from ai.minmax.alphabeta_ai import AlphaBetaAI
from ai.neural_network.neural_network import NeuralNetwork

MODE_PVP = 0
MODE_AI_NN = 1
MODE_AI_AB = 2

DELAY_BETWEEN_GAME_SECONDS = 1.5


class GameManager:

    def __init__(self, game):
        self.server = None
        self.ai_nn = None
        self.ai_ab = None

        self.current_mode = 0

        self.game = game

        self.done = False

    def start_game(self):
        self.game.start_new_game()

        # Player vs Player
        if self.current_mode is MODE_PVP:
            self.set_game_size(7, 6)
            self.game.start_new_game()

            self.play_game(p1=self.server, p2=self.server)

        # Neural Network
        elif self.current_mode is MODE_AI_NN:
            self.set_game_size(7, 6)
            self.game.start_new_game()

            if self.ai_nn is None:
                self.init_ai_nn()

            self.play_game(p1=self.server, p2=self.ai_nn)

        # Alpha Beta
        elif self.current_mode is MODE_AI_AB:
            self.set_game_size(4, 4)
            self.game.start_new_game()

            if self.ai_ab is None:
                self.init_ai_ab()
            else:
                self.ai_ab.reset()

            self.play_game(p1=self.server, p2=self.ai_ab)

    def play_game(self, p1, p2):
        self.done = False

        while not self.done:
            if self.game.player is -1:
                p1.play()
            else:
                p2.play()

            with self.game.lock:
                if self.game.is_done():
                    self.done = True
                    time.sleep(DELAY_BETWEEN_GAME_SECONDS)

    def set_game_size(self, w, h):
        self.game.board_width = w
        self.game.board_height = h

    def start(self):
        while True:
            self.start_game()

    def restart(self):
        self.done = True

    def set_mode(self, mode):
        self.current_mode = mode

    def set_server(self, server):
        self.server = server

    def init_ai_nn(self):
        self.ai_nn = NeuralNetwork(self.game, player=1)

    def init_ai_ab(self):
        self.ai_ab = AlphaBetaAI(self.game, ai_player=1)
