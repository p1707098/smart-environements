
import random
from threading import Lock

from copy import deepcopy


class Connect4:

    ALIGNED_PAWNS_TO_WIN = 4

    def __init__(self):
        random.seed()

        self.board_width = 7
        self.board_height = 6

        self.player = None
        self.board = None
        self.winner = None
        self.last_move = None

        self.lock = Lock()

        self.stats = None
        self.results = []
        self.reset_stats()

    def start_new_game(self):
        self.board = self.new_board()
        self.winner = 0
        self.last_move = None
        self.player = -1

    def new_board(self):
        return [[0 for x in range(self.board_width)] for y in range(self.board_height)]

    @staticmethod
    def get_random_player():
        return 1 if random.random() > .5 else -1

    def switch_player(self):
        self.player = self.player * -1

    def is_done(self):
        return self.winner != 0

    def get_column_free_case(self, x):
        for y in reversed(range(self.board_height)):
            if self.board[y][x] is 0:
                return y
        return -1

    def possible_moves(self):
        moves = []
        for i in range(self.board_width):
            moves.append(self.board[0][i] is 0)
        return moves

    # -1 not playable, 0  played, 1 won, 2 draw
    def play(self, x):
        if 0 <= x < self.board_width and not self.is_done():
            free_case = self.get_column_free_case(x)
            if free_case is -1:
                return -1

            self.board[free_case][x] = self.player
            self.last_move = x

            if self.has_won(self.player, x, free_case):
                self.winner = self.player
                self.results.insert(0, self.winner)
                self.stats['p1_wins' if self.player is -1 else 'p2_wins'] += 1
                self.player = 0
                self.stats['matches'] += 1
                return 1

            elif self.is_full():
                self.winner = 2
                self.results.insert(0, 0)
                self.player = 0
                self.stats['draws'] += 1
                self.stats['matches'] += 1
                return 2

            self.switch_player()
            return 0
        return -1

    def random_play(self):
        x = random.randint(0, self.board_width-1)
        while self.play(x) is -1:
            x = random.randint(0, self.board_width-1)

    def is_full(self):
        for i in range(self.board_width):
            if self.board[0][i] is 0:
                return False
        return True

    def has_won(self, player, x, y):
        # Row
        count = 0
        for xx in range(self.board_width):
            count = count+1 if self.board[y][xx] is player else 0
            if count >= Connect4.ALIGNED_PAWNS_TO_WIN:
                return True
        # Column
        count = 0
        for yy in range(self.board_height):
            count = count+1 if self.board[yy][x] is player else 0
            if count >= Connect4.ALIGNED_PAWNS_TO_WIN:
                return True
        # Decreasing diagonal
        count, i, j = 1, 1, 1
        while y+i < self.board_height and x+i < self.board_width and self.board[y+i][x+i] == player:
            count, i = count+1, i+1
        while y-j >= 0 and x-j >= 0 and self.board[y-j][x-j] == player:
            count, j = count+1, j+1
        if count >= Connect4.ALIGNED_PAWNS_TO_WIN:
            return True
        # Increasing diagonal
        count, i, j = 1, 1, 1
        while y+i < self.board_height and x-i >= 0 and self.board[y+i][x-i] == player:
            count, i = count+1, i+1
        while y-j >= 0 and x+j < self.board_width and self.board[y-j][x+j] == player:
            count, j = count+1, j+1
        if count >= Connect4.ALIGNED_PAWNS_TO_WIN:
            return True

        return False

    def reset_stats(self):
        self.stats = {
            'p1_wins': 0,
            'p2_wins': 0,
            'draws': 0,
            'matches': 0
        }
        self.results = []

    def get_status(self):
        return {
            'board': self.board,
            'winner': self.winner,
            'stats': self.stats,
            'player': self.player,
            'results': self.results
        }

    def __deepcopy__(self, memo):
        # Deepcopy des attributs excepté pour lock qui génère une erreur sinon
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if k != "lock":
                setattr(result, k, deepcopy(v, memo))
        return result
