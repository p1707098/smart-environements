
new Vue({
    el: '#app',
    data: {
        board: [],
        winner: 0,
        player: 0,
        stats: {
            'p1_wins': 0,
            'p2_wins': 0,
            'draws': 0,
            'matches': 0
        },
        results: [],
        loading: false
    },
    methods: {
        updateBoard: function () {
            fetch('/board')
                .then(response => {
                    response.json().then(status => {
                        this.board = status.board
                        this.winner = status.winner
                        this.stats = status.stats
                        this.player = status.player
                        this.results = status.results
                    })
                })
        },
        play: function (x) {
            fetch('/board', {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                }),
                body: `x=${x}`
            }).then(_ => this.updateBoard())
        },
        startNewGame: function (mode) {
            fetch('/start',{
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                }),
                body: `mode=${mode}`
            }).then(_ => this.updateBoard())

            if (mode === 2) {
                this.loading = true
                setTimeout(_ => { this.loading = false }, 5000)
            }
        },
        resetStats: function () {
            fetch('/reset', {
                method: 'PUT'
            }).then(_ => this.updateBoard())
        }
    },
    created: function () {
        this.updateBoard()
        setInterval(this.updateBoard, 200)
    }
})




