
import threading
from flask import Flask, request, jsonify

import logging

from ai.player_agent import PlayerAgent

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


class Server(threading.Thread, PlayerAgent):
    # Flask server running in a separate thread
    def __init__(self):
        super(Server, self).__init__()
        self.app = Flask(__name__, static_url_path='/static/')
        self.game = None
        self.game_manager = None

        # True when it is the server turn (received from play function)
        self.can_play = False

        # Web Thing API player presence
        self.wt_is_player_present = False

        @self.app.route('/')
        def root():
            return self.app.send_static_file("index.html")

        @self.app.route('/board', methods=['GET', 'POST'])
        def board():
            # POST request made when playing a move
            if request.method == 'POST':
                if self.can_play:
                    # column to play in from request
                    x = int(request.form['x'])

                    played = self.game.play(x)
                    self.can_play = False
                    if self.game.lock.locked():
                        self.game.lock.release()

                    if played is -1:
                        return '-1', 401
                    else:
                        return str(played), 202

                else:
                    return '-1', 401

            else:
                # GET request to retrieve game status
                return jsonify(self.game.get_status())

        @self.app.route('/board/last')
        def get_last_move():
            if self.game.last_move is None:
                last = -1
            else:
                last = self.game.last_move

            return str(last) + str(self.game.winner)

        @self.app.route('/start', methods=['GET', 'POST'])
        def start_game():
            if request.method == 'POST':
                mode = int(request.form['mode'])
            else:
                mode = 0

            self.game_manager.set_mode(mode)
            self.game_manager.restart()

            if self.game.lock.locked():
                self.game.lock.release()

            player = str(self.game.player)
            return player, 205

        @self.app.route('/reset', methods=['PUT'])
        def reset():
            self.game.reset_stats()
            self.game.start_new_game()
            return '', 205

        @self.app.route('/app.js')
        def js():
            return self.app.send_static_file("app.js")

        @self.app.route('/styles.css')
        def css():
            return self.app.send_static_file("styles.css")

        # Web Thing API
        @self.app.route('/thing')
        def get_thing_description():
            return jsonify({
                'id': 0,
                'description': 'Connect 4 connected board',
                'name': 'Connect 4',
                'properties': {
                    'presence': {
                        'type': 'boolean',
                        'description': 'Wether the player is present or not',
                        'links': [{'href': '/thing/properties'}]
                    }
                }
            })

        @self.app.route('/thing/properties', methods=['POST', 'GET'])
        def update_player_presence():
            if request.method == 'POST':
                presence = int(request.form['presence'])
                self.wt_is_player_present = True if presence is 1 else False

            return jsonify({'presence': 'true' if self.wt_is_player_present else 'false'})

    def set_game(self, game):
        self.game = game

    def set_game_manager(self, gm):
        self.game_manager = gm

    def play(self):
        self.can_play = True
        self.game.lock.acquire()

    def run(self):
        self.app.run(threaded=True)
