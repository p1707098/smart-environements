#!/usr/bin/python3.5

from server.server import Server
from connect4.connect4 import Connect4
from connect4.game_manager import GameManager


def start():
    game = Connect4()
    game_manager = GameManager(game)

    server = Server()
    server.set_game(game)
    server.set_game_manager(game_manager)

    server.start()
    game_manager.set_server(server)

    game_manager.start()


if __name__ == '__main__':
    start()
