# Jeu du Puissance 4 connecté
### Master 2 IA - Smart Environments

Elise Guiraud, Etienne Lefebvre, Nicolas Loiseau--Witon, Gauthier Magnin


## Description

Le jeu du Puissance 4 Connecté permet de faire des parties de Puissance 4 sur une planche physique et de
la visualiser sur une matrice de LEDs. Une interface Web offre des statistiques et une visualisation en
direct, ainsi que quelques contrôles.


## Structure du projet

`connect4/` : Modélisation des parties, des règles et des méthodes de jeu.

`server/` : Les fichiers statiques et composantes Flask du serveur Python.

`ai/` : Les implémentations par réseau de neurones, MinMax, et AlphaBeta de l'intelligence artificiellle.

`arduino/` : Fichiers spécifiques aux board Arduino et NodeMCU (micro-contrôleur WiFi).


## Installation et Lancement

### Installation du serveur

Le serveur nécessite une version de Python compatible avec Tensorflow 
(Python 3.4, Python 3.5, Python 3.6).

1. Création d'un environnement virtuel et installation des dépendances

    ```bash
    virtualenv --system-site-packages -p python3 ./venv
    source ./venv/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt 
    ```

2. Lancement du serveur

    ```bash
    python3 app.py
    ```
    
3. Le serveur est accessible à l'adresse `localhost:5000`.
    
    
### Installation Arduino

Avec un programme permettant de flasher les boards Arduino (comme l'[IDE Arduino](https://www.arduino.cc/en/main/software)),
flasher les fichiers `esp8266.ino` sur le micro-controlleur WiFi et `arduino_uno.ino` sur l'Arduino Uno.

Si les branchements sont corrects et le serveur tourne, le Puissance 4 connecté est prêt à fonctionner.


## Résultats

La partie serveur et la modélisation du jeu fonctionnent correctement, il est possible de faire
des parties en mode 2 joueurs, et de choisir parmi les IAs disponibles.

L'interface Web permet de visualiser la partie en temps réel, de jouer comme depuis la planche de
jeu physique, et affiche des statistiques et résultats des parties. Le serveur dispose d'une interface 
REST qui permet de jouer, de récupérer l'état courant du jeu et les statistiques, et de redémarrer la
partie.

La planche physique est équipée d'un joystick pour déplacer le pion à jouer et de boutons pour relancer
le jeu et placer le pion. La matrice de LEDs affiche la partie en cours et anime les nouveaux pions et les 
fins de parties quand le joueur gagne, perd, ou qu'il y a égalité.

L'IA implémentée avec le réseau de neurones n'est pas très performante et nécessiterait de trouver
des hyper-paramètres plus adaptés ou d'être entrainée sur un jeu de données de parties optimales.

Les IAs implémentées avec les algorithmes MinMax et AlphaBeta fonctionnent sur des planches de petites tailles
(4 x 5) ou avec un petit nombre de pions à aligner (< 4). Leur initialisation sur de plus grandes planches 
demande beaucoup de mémoire et de temps pour calculer l'arbre (ou une partie de cet arbre) des possibilités.

#### REST API et Web of Things
    
|    Endpoint   | Méthode |                        Description                        | Paramètres |                       Réponse                      |
|:-------------:|:------:|:---------------------------------------------------------:|:----------:|:--------------------------------------------------:|
| `/board`      | `GET`  | Récupérer l'état courant de la partie et les statistiques |      -     | 200 : JSON                                         |
| `/board`      | `POST` | Jouer un nouveau pion à la position `x`                   | `x = int`  | 202 : `int` Played, Won, Draw ; 401 : Forbidden move |
| `/board/last` | `GET`  | Obtenir la dernière position jouée ou `-1`                |            | 200 : `int` Position                               |
| `/start`      | `POST` | Commencer une nouvelle partie en mode `m`                 | `mode = m` | 205 : Game restarted                               |
| `/reset`      | `PUT`  | Réinitialiser les scores                                  |            | 205 : Scores reseted                               |
| `/thing`      | `GET`  | Récupérer l'état WoT de l’objet                           |            | 200 : `JSON`                                       |
| `/thing/properties`      | `GET`  | Récupérer les valeurs des propriétés           |            | 200 : `JSON`                                       |
| `/thing/properties`      | `POST` | Mettre à jour les propriétés                   | `JSON: properties` | 202 : `JSON`                               |


## Difficultés

### Affichage avec LEDs

La première solution était d'utiliser les LEDs disponibles. L'architecture électronique correspondait donc
à un ensemble de 6 x 7 x 2 LEDs : une pour chacune des deux couleurs, pour chaque case de la grille. Cela
aurait nécessité un nombre de connexions beaucoup trop important pour le micro-contrôleur Arduino. Nous
avons donc investi dans une matrice de LEDs constituée de 8 x 8 LEDs multicolors. Le nombre de fils connectés
à cette matrice n'est alors que de 3.

### Intelligence Artificielle

L'implémentation de l'intelligence avec le réseau de neurones atteint son plafond d'apprentissage 
après environ 100 000 parties contre elle-même. Son niveau n'est pas assez satisfaisant pour offrir
un vrai défi à l'utilisateur (si celui-ci a plus de 6 ans). Une des solutions possible serait d'entraîner
le réseau actuel contre un jeu de données de parties optimales, mais nous n'en avons pas trouvé en ligne ; ou
de l'entraîner contre une autre IA "parfaite" (algorithme MinMax, AlphaBeta).

Les IA utilisant les algorithmes MinMax et AlphaBeta ont aussi été implémentées mais leur initialisation sur
de larges grilles (plus grande que 4 x 5, comme la grille classique de 7 x 6) est extrêmement coûteuse en
mémoire et en temps. La complexité du modèle s'explique à la fois par le facteur de branchement de l'arbre créé
(correspondant au nombre d'actions possibles, au maximum de 7), ainsi que du nombre de noeuds de l'arbre complet
(correspondant au nombre d'états différents engendré par les 42 cases). La création de l'arbre des possibilités
est relativement rapide pour des grilles de taille inférieure à 4 x 4, mais nécessiterait de nombreuses heures
pour la taille de grille classique (7 x 6).

Un solution serait d'utiliser l'algorithme MinMax pour générer une unique fois les deux arbres complets des
possibilités (qui diffèrent par le joueur qui commence la partie) et de les enregistrer pour pouvoir charger
le bon à chaque début de partie.


## Outils utilisés

- [Arduino & Arduino IDE](https://www.arduino.cc/en/main/software)

- [Flask](http://flask.pocoo.org/)

- [Tensorflow](https://tensorflow.org)

- [Vue JS](https://vuejs.org/)

- [Spectre](https://picturepan2.github.io/spectre/)
