
from ai.neural_network.neural_network import NeuralNetwork

from connect4.connect4 import Connect4


def train_ai_self(n=1000000):
    game = Connect4()
    last_stats = game.stats.copy()

    ai1 = NeuralNetwork(game, randomness=.15, player=-1)
    ai2 = NeuralNetwork(game, randomness=.15, player=1)

    print("Training network (", n, "iterations )...")
    for i in range(n):
        play_game(game, ai1, ai2)

        ai1.game_feedback(game.winner)
        ai2.game_feedback(game.winner)

        if i > 0 and i % 25000 == 0:
            # Save graph to /ai/saved_graphs
            ai2.save_graph()
            ai1.save_graph()

            print('#{:8}  D: {:+5d}, P1: {:+5d}, P2: {:+5d}, P1-P2: {:4}   {}'.format(
                i,
                game.stats['draws'] - last_stats['draws'],
                game.stats['p1_wins'] - last_stats['p1_wins'],
                game.stats['p2_wins'] - last_stats['p2_wins'],
                game.stats['p1_wins'] - game.stats['p2_wins'],
                game.stats))
            last_stats = game.stats.copy()
            game.reset_stats()


def play_game(game, p1, p2):
    done = False

    while not done:
        if game.player is -1:
            p1.play()
        else:
            p2.play()

        with game.lock:
            done = game.is_done()


train_ai_self()
