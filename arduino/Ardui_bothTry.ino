//Arduino code
#include <SoftwareSerial.h>
SoftwareSerial s(5,6);
//SoftwareSerial s(18,19);
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 4
#define NUM_LEDS 64
#define BRIGHTNESS 20
#define NBLED 65
#define LED 8
#define PLAYER 1
#define OFF 0
#define PC 2
#define WINK 3
#define IA 102
#define PVP 100
#define NEURONE 101
#define EGALITE 2
#define GAGNE 1
#define PASPOSSIBLE -1

//Bouton
const int pinBouton = 7;
const int pinBoutonIA = 9;
const int pinBoutonPvP = 8;

// analogue
const int SW_pin = 2; // digital pin connected to switch output
const int X_pin = 0; // analog pin connected to X output
const int Y_pin = 1; // analog pin connected to Y output

// PIR
int ledPin = 13;                // choose the pin for the LED
//int inputPin = 7;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status

//Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

uint16_t matrix[8][8]={ {0,1,2,3,4,5,6,7},{15,14,13,12,11,10,9,8},{16,17,18,19,20,21,22,23},{31,30,29,28,27,26,25,24},{32,33,34,35,36,37,38,39},{47,46,45,44,43,42,41,40},{48,49,50,51,52,53,54,55},{63,62,61,60,59,58,57,56} };

// 
int color[8][8];
bool first = false;

uint32_t r = strip.Color(255, 0, 0);
uint32_t g = strip.Color(0, 255, 0);
uint32_t b = strip.Color(0, 0, 255);
uint32_t o = strip.Color(0, 0, 0);
uint32_t yellow = strip.Color(255, 255, 0);

bool choixPartie = false;

int choixMode; 
//O : Réseau Neurones
//1 : IA MinMax
//2 : PvP

int LARGEUR;
int HAUTEUR;

bool ok = false;
 
void setup() {
  s.begin(9600);
  Serial.begin(9600);
  for(int i=0; i<LED; i++) {
    for(int j=0; j<LED; j++) {
      color[i][j] = OFF;
    }
  }
  //PIR 
  pinMode(ledPin, OUTPUT);      // declare LED as output
  //pinMode(inputPin, INPUT);     // declare sensor as input

  // Analog
  pinMode(SW_pin, INPUT);
  digitalWrite(SW_pin, HIGH);

  //Bouton
  pinMode(pinBouton,INPUT);
  pinMode(pinBoutonIA, INPUT);
  pinMode(pinBoutonPvP, INPUT);

  //Matrix
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}
 
void loop() {
  while(choixPartie==false){
    offAll();
    Serial.println("attend choix partie");
    // choix IA Neurone
    if(digitalRead(pinBouton)){
      Serial.print("bouton IA Random :");
      Serial.println(digitalRead(pinBouton));
      choixPartie = true;
      LARGEUR = 7;
      HAUTEUR = 6;
      choixMode = 0;
      s.write(NEURONE);
      delay(1000);
    }
    //choix IA 
    if(digitalRead(pinBoutonIA)){
      Serial.print("bouton IA :");
      Serial.println(digitalRead(pinBoutonIA));
      choixPartie = true;
      LARGEUR = 4;
      HAUTEUR = 4;
      choixMode = 1;
      s.write(IA);
      delay(6000);
    }
    //choix PvP
    if(digitalRead(pinBoutonPvP)){
      Serial.print("bouton PvP :");
      Serial.println(digitalRead(pinBoutonPvP));
      choixPartie = true;
      LARGEUR = 7;
      HAUTEUR = 6;
      choixMode = 2;
      s.write(PVP);
      delay(1000);
    }
  }
  //IA NEURONE OU MIN MAX
  if(choixMode == 0 || choixMode == 1){
    Serial.print("choix Mode = 0 ou 1, ok = ");
    Serial.println(ok);
    while(ok == false){
      int p = choixPosition(true);
      int data=50;
      //Serial.println("Arduino sAvai");
      while(!s.isListening()){
        
      }
      s.write(p);
      
      Serial.print("envoi = ");
      Serial.println(p);
      
      delay(1000);

      if(p == IA){
        Serial.println("Attente de 5 secondes");
        delay(5000);
      }

      if(p<7 && p>=0){
        Serial.println("p<7 && p>=0");
        ok = true;
        
        while(s.available()!=1){
          Serial.println("attend");
        }
        Serial.print("reçu : ");
        
        data=s.read();

        Serial.println(data);
        
        if(data == PASPOSSIBLE){
          ok = false;
        }
        else if(data == EGALITE){
          offAll();
          colorWipe(strip.Color(255, 0, 0), 90); // Red
          choixPartie = false;
          ok=false;
        }
        else if(data == GAGNE){
          offAll();
          rainbow(20);
          choixPartie = false;
          ok=false;
        }
        else
        {
          tombePC((data - data%10)/10);
          ok=false;
          if(data%10 == EGALITE){ //perdu
            offAll();
            colorWipe(strip.Color(255, 0, 0), 90); // Red
            choixPartie = false;
            ok=false;
          }
          else if(data%10 == GAGNE){ //gagné
            offAll();
            rainbow(20);
            choixPartie = false;
            ok=false;
          }
        }
      }
    }
  }
  else if (choixMode == 2){
    Serial.print("choix Mode = 2, ok = ");
    Serial.println(ok);
    int p1 = choixPosition(true);
    
    while(!s.isListening()){
      Serial.println("Attend si Listening");
    }
    Serial.print("envoie : ");
    Serial.println(p1);
    s.write(p1);

    
    
    //delay(1000);

    if(p1<7 && p1>=0){
              Serial.println("TEST P1");
              while(s.available()!=1){
                Serial.println("attend choixMode =2");
              }
              int data=s.read();
              if(data == EGALITE){ //perdu
                offAll();
                colorWipe(strip.Color(255, 0, 0), 90); // Red
                //offAll();
                choixPartie = false;
                ok=false;
              }
              else if(data == GAGNE){ //gagné
                offAll();
                rainbow(20);
                choixPartie = false;
                ok=false;
              }
              else if(data!=PVP){
                int p2 = choixPosition(false);
                while(!s.isListening()){
                }
                s.write(p2);
                //delay(1000);

                if(p2<7 && p2>=0){
                        Serial.println("TEST P2");
                        while(s.available()!=1){
                          Serial.println("attend choix mode 2 P2");
                        }
                        data=s.read();
                        if(data == EGALITE){ //perdu
                          offAll();
                          colorWipe(strip.Color(255, 0, 0), 90); // Red
                          choixPartie = false;
                          ok=false;
                        }
                        else if(data == GAGNE){ //gagné
                          offAll();
                          rainbow(20);
                          choixPartie = false;
                          ok=false;
                        }
                }
              }
        }
    
              
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}


uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

int choixPosition(bool joueur1){
  Serial.println("Choix Position");
  int x=0;int analog;
  boolean etat=0;
  while(digitalRead(SW_pin)==1 && digitalRead(pinBouton)==0 && digitalRead(pinBoutonIA)==0 && digitalRead(pinBoutonPvP) == 0){
      analog = analogRead(Y_pin);
      if(joueur1){
        wink(x, HAUTEUR, PLAYER);
      }else{
        wink(x, HAUTEUR, PC);
      }
      if(analog>600){
        if(x<LARGEUR-1) x++;
      }else if(analog<450){
        if(x>0) x--;
      }
  }
  if(digitalRead(pinBouton)){//Reset NEURONE
      Serial.println("NEURONE");
      offAll();
      choixMode = 0;
      LARGEUR = 7;
      HAUTEUR = 6;
      ok=false;
      return NEURONE;
  }
  else if (digitalRead(pinBoutonIA)){ //Reset IA
      Serial.println("IA");
      offAll();
      choixMode = 1;
      LARGEUR = 4;
      HAUTEUR = 4;
      ok=false;
      return IA;
  }
  else if(digitalRead(pinBoutonPvP)){
    Serial.println("PvP");
    offAll();
    choixMode = 2;
    LARGEUR = 7;
    HAUTEUR = 6;
    ok=false;
    return PVP;
  }
  if(joueur1){
    tombe(x);
  }
  else{
    tombePC(x);
  }
  return x;
}

void offAll() {
  for(int i=0; i<LED; i++) {
    for(int j=0; j<LED; j++) {
      color[i][j] = OFF;
      strip.setPixelColor(matrix[i][j], o);
    }
  }
  strip.show();
}

void wink(int x, int y, int couleur){
  int tmp = color[x][y];
  color[x][y] = couleur;
  toScreenMatrix();
  delay(100);
  color[x][y] = tmp;
  toScreenMatrix();
}

void toScreenMatrix() {
  for(int i=0; i<LED; i++) {
    for(int j=0; j<LED; j++) {
      if(color[i][j] == PLAYER)
        strip.setPixelColor(matrix[i][j], yellow);
      else if(color[i][j] == PC)
        strip.setPixelColor(matrix[i][j], r);
      else if(color[i][j] == OFF)
        strip.setPixelColor(matrix[i][j], o);
      else if(color[i][j] == WINK)
        strip.setPixelColor(matrix[i][j], b);
      strip.show();
    }
  }
  
}

void tombe(int i){
  int bas =HAUTEUR;
  int tmp = OFF;
  //Serial.print(i);
  tmp = color[i][bas];
  while(tmp != PLAYER && tmp != PC && bas != -1){
    if(color[i][bas]==PLAYER || color[i][bas]==PC){
      tmp = PLAYER;
    }else{
      tmp = color[i][bas];
      bas--;
    }    
  }

  //Serial.print(bas);
  for(int k=HAUTEUR; k>bas; k--)
  {
    color[i][k] = PLAYER;
    toScreenMatrix();
    //delay(1);
    if(k != bas+1){
      color[i][k] = OFF;
      toScreenMatrix();
    }
  }
}

void tombePC(int i){
  int bas =HAUTEUR;
  int tmp = OFF;
  //Serial.print(i);
  tmp = color[i][bas];
  while(tmp != PLAYER && tmp != PC && bas != -1){
    if(color[i][bas]==PLAYER || color[i][bas]==PC){
      tmp = PC;
    }else{
      tmp = color[i][bas];
      bas--;
    }    
  }

  //Serial.print(bas);
  for(int k=HAUTEUR; k>bas; k--)
  {
    color[i][k] = PC;
    toScreenMatrix();
    //delay(1);
    if(k != bas+1){
      color[i][k] = OFF;
      toScreenMatrix();
    }
  }
}

void colorWipe(int table[NBLED]) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, r);
    //Serial.print(i);
    //Serial.print(" ");
  }
  delay(100);
  strip.show();
  //Serial.println();
}

void onepoint(uint32_t c, uint8_t wait) {
  uint16_t i=0, j=1;
  strip.setPixelColor(i, c);
  strip.setPixelColor(j, c);
  strip.setPixelColor(9, c);
  strip.setPixelColor(16, c);
  strip.show();
}
