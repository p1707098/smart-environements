#include <SoftwareSerial.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

SoftwareSerial s(D6,D5);

#define IA 102
#define NEURONE 101
#define PVP 100
#define RST 20
#define PERDU 10
#define GAGNE 11

int webThingID = 0;
int inputPin = 16;               // choose the input pin (for PIR sensor)
int ledPin = 13;               // LED
int data;
long randNumber;
int choix;
int choixMode=0;
String payload="0";
int pirVal;
String retPost="0";
String a;

HTTPClient http;    //Declare object of class HTTPClient

void setup() {
	Serial.begin(9600);
	s.begin(9600);
	delay(500);

	randomSeed(analogRead(0));

	Serial.println("#######################################################");
	Serial.println("####         LANCEMENT DU SERVEUR (NODE MCU)       ####");
	Serial.println("#######################################################");

	WiFi.begin("Oasis-TD8", "@Deay2dh&");        //WiFi connection
	//WiFi.begin("Cmoi", "azerty1234");        //WiFi connection
	//WiFi.begin("Livebox-5CD8", "1234fois2");        //WiFi connection
	while (WiFi.status() != WL_CONNECTED) {         //Wait for the WiFI connection completion
		delay(500);
		Serial.println("Waiting for connection");
	}
	Serial.println("#### WIFI OK.");
	pinMode(inputPin, INPUT); // PIR en 7
	pinMode(ledPin, OUTPUT);
	digitalWrite(ledPin, LOW);
	Serial.println("#### PIR OK.");
}

void loop() {
	Serial.println("#######################################################");
	randNumber = random(7);
///webThing {"led"=true}

	Serial.println("#### En attente de la case du joueur");
	int k=0;
	while (s.available()!=1)
	{
		

		//delay(1000);
		if(k%100==0){
			pirVal = digitalRead(inputPin); // read input value
			/*http.begin("http://connect4.etiennelefebvre.com/thing/properties");      //Specify request destination
			http.addHeader("Content-Type", "application/x-www-form-urlencoded");
			if(pirVal == 1){
				digitalWrite(ledPin, HIGH);
				a = "presence=1";
			}else{
				digitalWrite(ledPin, LOW);
				a = "presence=0";
			}
			int httpCode = http.POST(a);   //Send the request
			http.end();  //Close connection*/
		}
		k++;
		yield();
	}

	data=s.read();
	Serial.print("\n#### Chiffre Reçu de l'arduino : ");
	Serial.println(data);



	if(WiFi.status()== WL_CONNECTED){
		if(data <7 && data>=0){


			if(choixMode == PVP){
				Serial.println("#### Envoie de la case au serveur");
				http.begin("http://connect4.etiennelefebvre.com/board");      //Specify request destination
				http.addHeader("Content-Type", "application/x-www-form-urlencoded");
				a = "x="+String(data);
				int httpCode = http.POST(a);   //Send the request		 
				
				payload = http.getString();                  //Get the response payload
				Serial.print("#### Chiffre reçu du serveur : ");
				Serial.println(payload);    //Print request response payload
				s.write(payload.toInt());
				int data=50;
				if(s.available()>0)
					s.write(data);
				Serial.print("#### On renvoie le chiffre ");
				Serial.println(" à l'arduino.");
			}else{

				Serial.println("#### Envoie de la case au serveur");
				http.begin("http://connect4.etiennelefebvre.com/board");      //Specify request destination
				http.addHeader("Content-Type", "application/x-www-form-urlencoded");
				a = "x="+String(data);
				int httpCode = http.POST(a);   //Send the request		 
				payload = http.getString();
				Serial.println(payload);    //Print request response payload
				http.end();  //Close connection

				if(payload.toInt()==-1){
					s.write(payload.toInt());
					int data=50;
					if(s.available()>0)
						s.write(data);
				}else if(payload.toInt()==0){
					//Get the response payload
					Serial.print("#### Chiffre reçu du serveur : ");
					Serial.println(payload);    //Print request response payload
					http.begin("http://connect4.etiennelefebvre.com/board/last");      //Specify request destination
					http.addHeader("Content-Type", "application/x-www-form-urlencoded");

					Serial.println("#### Requete get.");
					int httpCodeGet = http.GET();
					
						String payload = http.getString();   //Get the request response payload
						//Serial.println(payload);                     //Print the response payload
					Serial.println("#### Requete get end.");
					Serial.print(payload);
					http.end();  //Close connection
					/*s.write(payload.toInt());
					int data=50;
					if(s.available()>0)
					s.write(data);*/
					//pch = strtok (str," ,.-");


					s.write(payload.toInt());
					data=50;
					if(s.available()>0)
					s.write(data);

				}else if(payload.toInt()==1){
					Serial.print("1");
					s.write(1);
					data=50;
					if(s.available()>0){
						Serial.print("avai");
						s.write(data);
					}

				}else if(payload.toInt()==2){

					s.write(payload.toInt());
					data=50;
					if(s.available()>0)
						s.write(data);

				}

				
			
			



			


			
			}


/*
#define IA 102
#define NEURONE 101
#define PVP 100
*/
		}else if(data == NEURONE){
			// réseau de neurone = 1
			Serial.println("#### Mode de jeu Reseau de neurone.");
			http.begin("http://connect4.etiennelefebvre.com/start");      //Specify request destination
			http.addHeader("Content-Type", "application/x-www-form-urlencoded");

			a = "mode=1";
			int httpCode = http.POST(a);   //Send the request
			retPost = http.getString();                  //Get the response payload
			Serial.print("#### Chiffre reçu du serveur : ");
			Serial.println(retPost);    //Print request response payload
			choixMode = NEURONE;

		}else if(data == PVP){
			// PVP 0

			Serial.println("#### Mode de jeu PVP.");
			http.begin("http://connect4.etiennelefebvre.com/start");      //Specify request destination
			http.addHeader("Content-Type", "application/x-www-form-urlencoded");

			a = "mode=0";
			int httpCode = http.POST(a);   //Send the request
			retPost = http.getString();                  //Get the response payload
			Serial.print("#### Chiffre reçu du serveur : ");
			Serial.println(retPost);    //Print request response payload
			choixMode = PVP;

		}else if(data == IA){
			// last 2 Alpha beta
			Serial.println("#### Mode de jeu Alpha Beta.");
			http.begin("http://connect4.etiennelefebvre.com/start");      //Specify request destination
			http.addHeader("Content-Type", "application/x-www-form-urlencoded");

			a = "mode=2";
			int httpCode = http.POST(a);   //Send the request
			payload = http.getString();                  //Get the response payload
			Serial.print("#### Chiffre reçu du serveur : ");
			Serial.println(retPost);    //Print request response payload
			choixMode = IA;
		}

	}else
		Serial.println("#### Error in WiFi connection.");   
	
	http.end();  //Close connection
	//delay(1000);  //Send a request every 30 seconde





	Serial.println("#######################################################");
	Serial.println("####              Nouvelle itération               ####");
	Serial.println("#######################################################");
	Serial.println("");
	Serial.println("");
}
