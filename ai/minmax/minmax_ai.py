# -*- coding: utf-8 -*-

from ai.minmax.minmax import Node
from ai.player_agent import PlayerAgent


class MinMaxAI(PlayerAgent):
    
    def __init__(self, game, ai_player, start_player):
        self.game = game
        self.ai_player = ai_player
        self.start_player = start_player
        
        self.state = Node(self.game, self.ai_player, self.start_player == self.ai_player)

    def play(self):
        # Descente dans l'arbre pour l'action du joueur
        if self.game.last_move is not None:
            self.state = self.state.get_child(self.game.last_move)
        
        # Recherche de la meilleur action et joue un coup
        column = self.state.next_move()
        
        self.game.play(column)
        self.state = self.state.get_child(column)