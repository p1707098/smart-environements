# -*- coding: utf-8 -*-

import copy

from connect4.connect4 import Connect4


class Node:
    
    def __init__(self, game, expected_winner, ia_turn, parent = None):
        # Etat du jeu, numéro de joueur de l'IA, à qui le tour et noeud parent
        self.game = game
        self.expected_winner = expected_winner
        self.ia_turn = ia_turn
        self.parent = parent
        
        self.score = -2         # Correspond à un score non défini
        
        # Création des noeuds fils
        self.children = None
        self.compute_children()
    
    
    def compute_children(self):
        if not self.game.is_done():
            self.children = []
            
            # Génération de toutes les grilles de jeu possibles en un coup
            for c in range(0, self.game.board_width):
                game_copy = copy.deepcopy(self.game)
                played = game_copy.play(c)
                
                if played:
                    self.children.append(Node(game_copy, self.expected_winner, not self.ia_turn, self))
                else:
                    self.children.append(None)
            
            # Nécessité d'avoir instancié tous les fils avant de calculer le score
            self.compute_score()
        
        # Calcul du score si le noeud courant est une feuille
        else:
            self.compute_score()
    
    
    def compute_score(self):
        # Feuille de l'arbre : vérification du gagnant
        if self.children is None:
            if self.game.winner == 2:
                self.score = 0
            elif self.game.winner == self.expected_winner:
                self.score = 1
            else:
                self.score = -1
                
        # Noeud interne : utilisation des scores fils
        else:
            # Liste les scores fils
            scores = []
            for i in range(len(self.children)):
                if self.children[i] is not None:
                    scores.append(self.children[i].score)
            
            # Cas différent en fonction d'à qui est le tour
            if self.ia_turn:
                self.score = max(scores)
            else:
                self.score = min(scores)
       
        
    def next_move(self):
        # Recherche d'une meilleure action
        for c in range(0, self.game.board_width):
            if self.children[c] is not None and self.children[c].score == self.score:
                return c
    
    
    def print(self):
        print(self.game.board, self.score)
    
            
    def get_child(self, column):
        # Pour parcourir l'arbre
        if self.children is not None:
            return self.children[column]
        
        return False
    
    