# -*- coding: utf-8 -*-

try:
    from connect4.connect4 import Connect4
except ImportError:
    import sys
    sys.path.append('../../')
    from connect4.connect4 import Connect4

#from minmax import Node
from alphabeta import Node
#from alphabeta2 import Node

import time


AI_PLAYER = -1
OTHER_PLAYER = 1
START_PLAYER = AI_PLAYER

def start():
    game = Connect4()
    game.start_new_game(START_PLAYER)
    
    start_time = time.time()
    racine = Node(game, AI_PLAYER, START_PLAYER == AI_PLAYER)
#    racine = Node(game.board, AI_PLAYER, START_PLAYER == AI_PLAYER)
    racine.compute_score()
    print(str(time.time() - start_time) + "s")
    
    # BOARD_WIDTH, BOARD_HEIGHT, ALIGNED_PAWNS_TO_WIN => time
    # MinMax algorithm:
    #                       3, 3, 3 => 0.40s
    #                       4, 3, 3 => 42.66s
    # AlphaBeta algorithm:
    #                       3, 3, 3 => 0.0156s
    #                       4, 3, 3 => 0.0467s
    #                       4, 4, 3 => 0.4530s
    #                       4, 5, 3 => 0.6724s
    #
    #                       4, 4, 4 => 5.4294s
    #                       4, 6, 3 => 13.340s
    #                       5, 4, 3 => 89.769s
    #                       4, 5, 4 => 119.09s
    #                       7, 6, 4 => >3h
    # 
    # AlphaBeta without Connect4 deepcopy
    #                       3, 3, 3 => 0.0156s
    #                       4, 4, 3 => 0.9730s
    
    racine.print()


if __name__ == '__main__':
    start()