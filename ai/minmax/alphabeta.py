# -*- coding: utf-8 -*-

import copy

from connect4.connect4 import Connect4


class Node:
    
    POSITIVE_INFINITY = 1000
    NEGATIVE_INFINITY = -1000
    
    def __init__(self, game, expected_winner, ai_turn, parent = None):
        # Etat du jeu, numéro de joueur de l'IA, à qui le tour et noeud parent
        self.game = game
        self.expected_winner = expected_winner
        self.ai_turn = ai_turn
        self.parent = parent
        
        self.score = None
        self.children = None
    
    
    def compute_score(self, alpha = NEGATIVE_INFINITY, beta = POSITIVE_INFINITY):
        # Feuille de l'arbre : vérification du gagnant
        if self.game.is_done():
            if self.game.winner == 2:
                self.score = 0
            elif self.game.winner == self.expected_winner:
                self.score = 1
            else:
                self.score = -1
            
            return self.score
                
        # Noeud interne : utilisation des scores fils
        else:
            # Cas différent en fonction d'à qui est le tour
            if self.ai_turn:        # Noeud Max
                self.score = Node.NEGATIVE_INFINITY
                self.children = []
                
                # Génération des états de la grille de jeu possibles en un coup
                for c in range(0, self.game.board_width):
                    self.compute_child(c)
                    child = self.children[c]
                    
                    if child is not None:
                        child_score = child.compute_score(alpha, beta)
                        self.score = max(self.score, child_score)
                        
                        if self.score >= beta:
                            return self.score
                        
                        alpha = max(alpha, self.score)
                
            else:                   # Noeud Min
                self.score = Node.POSITIVE_INFINITY
                self.children = []
                
                # Génération des états de la grille de jeu possibles en un coup
                for c in range(0, self.game.board_width):
                    self.compute_child(c)
                    child = self.children[c]
                    
                    if child is not None:
                        child_score = child.compute_score(alpha, beta)
                        self.score = min(self.score, child_score)
                        
                        if self.score <= alpha:
                            return self.score
                        
                        beta = min(beta, self.score)
                
            return self.score
    
    
    def compute_child(self, column):
        # Définit l'état suivant pour l'action "column"
        # Nécessite que les enfants pour 0 <= c < column existent déjà
        
        game_copy = copy.deepcopy(self.game)
        can_play = game_copy.play(column)
        
        child = None
        if can_play != -1:
            child = Node(game_copy, self.expected_winner, not self.ai_turn, self)
        self.children.append(child)
        
        
    def next_move(self):
        # Recherche d'une meilleure action
        for c in range(0, self.game.board_width):
            if self.children[c] is not None and self.children[c].score == self.score:
                return c
    
    
    def print(self):
        print(self.game.board, self.score)
    
            
    def get_child(self, column, real_game):
        # Pour parcourir l'arbre
        
        if self.children is not None:
            # Coup prévu dans la construction de l'arbre
            if column < len(self.children):
                return self.children[column]
            # Sinon, calcul de la sous-partie de l'arbre associée
            else:
                return self.new_node(real_game)
        else:
            return self.new_node(real_game)
        
        # Something went wrong
        return False
    
    
    def new_node(self, real_game):
        # Définit un nouveau sous-espace de l'arbre (pas encore parcouru)
        
        game_copy = copy.deepcopy(real_game)
        child = Node(game_copy, self.expected_winner, not self.ai_turn)
        child.compute_score()
        return child
    