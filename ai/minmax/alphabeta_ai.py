# -*- coding: utf-8 -*-

import copy

from ai.minmax.alphabeta import Node as Node
from ai.player_agent import PlayerAgent


class AlphaBetaAI(PlayerAgent):
    
    def __init__(self, game, ai_player):
        self.game = game
        self.ai_player = ai_player
        
        game_copy = copy.deepcopy(game)
        self.state = Node(game_copy, ai_player, game.player == ai_player)
        self.state.compute_score()

    def play(self):
        # Descente dans l'arbre pour l'action du joueur
        if self.game.last_move is not None:
            self.state = self.state.get_child(self.game.last_move, self.game)
        
        # Recherche de la meilleur action et joue un coup
        column = self.state.next_move()
        
        self.game.play(column)
        self.state = self.state.get_child(column, self.game)

    def reset(self):
        game_copy = copy.deepcopy(self.game)
        ai_turn = self.game.player == self.ai_player
        
        self.state = Node(game_copy, self.ai_player, ai_turn)
        self.state.compute_score()