from ai.player_agent import PlayerAgent


class RandomPlayer(PlayerAgent):

    def __init__(self, game):
        self.game = game

    def play(self):
        self.game.random_play()
