
import tensorflow as tf

from ai.player_agent import PlayerAgent

from copy import deepcopy
import random

GRAPHS_DIRECTORY = 'ai/neural_network/saved_graphs/'


class NeuralNetwork(PlayerAgent):

    def __init__(self, game, player=-1,
                 randomness=.0, learning_rate=.005,
                 n_hidden=126, nb_hidden=2,
                 score_win=1, score_loss=-.25):

        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)

        self.player = player
        self.game = game
        self.randomness = randomness
        self.saved_actions = []

        self.learning_rate = learning_rate
        self.n_hidden = n_hidden
        self.nb_hidden = nb_hidden

        self.score_win = score_win
        self.score_loss = score_loss

        self.number_of_games = 0

        self.graph_file_name = "{}graph-{}x{}-{}-{}-{}"\
            .format(GRAPHS_DIRECTORY, game.board_width, game.board_height,
                    nb_hidden, n_hidden, 1 if self.player == -1 else 2)
        self.saver = None
        self.predict = None
        self.optimizer = None

        self.board = None

        self.rating = None
        self.cost = None

        with self.graph.as_default():
            self.construct_network()

        with tf.Session() as tmp_session:
            tmp_session.run(tf.global_variables_initializer())

        self.session.run(tf.variables_initializer(self.graph.get_collection('variables')))

        self.restore_graph()

    def construct_network(self):
        board_width = self.game.board_width
        board_height = self.game.board_height

        n_input = board_width * board_height * 3

        # tf Graph input
        self.board = tf.placeholder("float", [board_height, board_width])

        x_p1 = tf.cast(tf.equal(self.board, -1), "float")
        x_p2 = tf.cast(tf.equal(self.board, 1), "float")
        x_empty = tf.cast(tf.equal(self.board, 0), "float")

        x = tf.reshape(tf.concat(axis=0, values=[x_p1, x_p2, x_empty]), [1, n_input])
        self.rating = tf.placeholder("float", [board_width])
        y = tf.reshape(self.rating, [1, board_width])

        # Hidden layer construction
        def construct_layer(previous_layer, layer_input, layer_output):
            layer = tf.add(tf.matmul(previous_layer, tf.Variable(tf.random_normal([layer_input, layer_output]))),
                           tf.Variable(tf.random_normal([layer_output])))
            layer = tf.sigmoid(layer)
            return layer

        # Network input
        network_layer = construct_layer(x, n_input, self.n_hidden)
        # Hidden layers
        for i in range(self.nb_hidden):
            network_layer = construct_layer(network_layer, self.n_hidden, self.n_hidden)
        # Network output
        network_output = tf.matmul(network_layer, tf.Variable(tf.random_normal([self.n_hidden, board_width]))) + \
            tf.Variable(tf.random_normal([board_width]))
        self.predict = tf.nn.softmax(network_output)

        # Backward propagation
        self.cost = tf.reduce_mean(tf.square(y - self.predict))
        # self.cost = tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.predict, labels=y)
        # self.cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.predict, labels=y))

        self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate).minimize(self.cost)
        # self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.cost)

        self.saver = tf.train.Saver()

    def __enter__(self):
        return self

    def play(self):
        if random.random() < self.randomness:
            self.game.random_play()
        else:
            self.game.play(self.next_move())

    def set_player(self, player):
        self.player = player

    def next_move(self):
        predictions = self.session.run([self.predict], feed_dict={
            self.board: self.game.board,
        })
        legal_moves = self.game.possible_moves()

        score = 0
        column = -1
        for index, prediction in enumerate(predictions[0][0]):
            if prediction > score and legal_moves[index]:
                score = prediction
                column = index

        self.saved_actions.append({
            'board': deepcopy(self.game.board),
            'column': column,
        })

        return column

    def game_feedback(self, winner):
        self.number_of_games += 1

        if winner != 0 and winner != 2:
            score = self.score_win if winner == self.player else self.score_loss
            with self.graph.as_default():
                for action in self.saved_actions:
                    self.back_propagation(action['board'], action['column'], score)

        self.saved_actions = []

    def restore_graph(self):
        with self.graph.as_default():
            try:
                self.saver.restore(self.session, self.graph_file_name)
                print("Restored graph file", self.graph_file_name)
            except ValueError:
                print("Cannot restore graph file", self.graph_file_name)

    def save_graph(self):
        print("Saving session graph", self.graph_file_name)
        with self.graph.as_default():
            self.saver.save(self.session, self.graph_file_name)

    def back_propagation(self, game_board, column, score):
        output_data = self.game.BOARD_WIDTH * [0.]
        output_data[column] = score

        _, c = self.session.run([self.optimizer, self.cost], feed_dict={
            self.board: game_board,
            self.rating: output_data
        })

    def __exit__(self, t, value, traceback):
        self.session.close()
