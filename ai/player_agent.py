
from abc import ABC, abstractmethod


class PlayerAgent(ABC):

    @abstractmethod
    def play(self):
        ...

    def reset(self):
        pass
